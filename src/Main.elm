module Main exposing (main)

import Browser
import Browser.Dom exposing (Viewport)
import Draggable
import Draggable.Events exposing (onClick, onDragBy, onDragStart)
import Html exposing (Html)
import Html.Events
import Html.Attributes
import Math.Vector2 as Vector2 exposing (Vec2, getX, getY,vec2)
import Svg exposing (Svg)
import Svg.Attributes as Attr
import Svg.Events exposing (onMouseUp)
import Svg.Keyed
import Svg.Lazy exposing (lazy)
import Task

import Json.Decode as Decode exposing (Decoder)



main : Program () Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


type alias Box =
    { id : Id
    , position : Vec2
    , clicked : Bool
    }

type alias FullZoom = 
    { scaleFactor : Float
    , zoomCenter : Vec2
    }

type alias Id =
    String


makeBox : Id -> Vec2 -> Box
makeBox id position =
    Box id position False


dragBoxBy : Vec2 -> Box -> Box
dragBoxBy delta box =
    { box | position = box.position |> Vector2.add delta }


toggleClicked : Box -> Box
toggleClicked box =
    { box | clicked = not box.clicked }


type alias BoxGroup =
    { uid : Int
    , movingBox : Maybe Box
    , idleBoxes : List Box
    }


emptyGroup : BoxGroup
emptyGroup =
    BoxGroup 0 Nothing []


addBox : Vec2 -> BoxGroup -> BoxGroup
addBox position ({ uid, idleBoxes } as group) =
    { group
        | idleBoxes = makeBox (String.fromInt uid) position :: idleBoxes
        , uid = uid + 1
    }


makeBoxGroup : List Vec2 -> BoxGroup
makeBoxGroup positions =
    positions
        |> List.foldl addBox emptyGroup


allBoxes : BoxGroup -> List Box
allBoxes { movingBox, idleBoxes } =
    movingBox
        |> Maybe.map (\a -> a :: idleBoxes)
        |> Maybe.withDefault idleBoxes


startDragging : Id -> BoxGroup -> BoxGroup
startDragging id ({ idleBoxes, movingBox } as group) =
    let
        ( targetAsList, others ) =
            List.partition (.id >> (==) id) idleBoxes
    in
    { group
        | idleBoxes = others
        , movingBox = targetAsList |> List.head
    }


stopDragging : BoxGroup -> BoxGroup
stopDragging group =
    { group
        | idleBoxes = allBoxes group
        , movingBox = Nothing
    }


dragActiveBy : Vec2 -> BoxGroup -> BoxGroup
dragActiveBy delta group =
    { group | movingBox = group.movingBox |> Maybe.map (dragBoxBy delta) }


toggleBoxClicked : Id -> BoxGroup -> BoxGroup
toggleBoxClicked id group =
    let
        possiblyToggleBox box =
            if box.id == id then
                toggleClicked box

            else
                box
    in
    { group | idleBoxes = group.idleBoxes |> List.map possiblyToggleBox }


type alias Model =
    { zoom : Float
    , panning : Bool
    , center : Vec2
    , boxGroup : BoxGroup
    , drag : Draggable.State Id
    , viewport : Maybe Viewport
    }


type Msg
    = DragMsg (Draggable.Msg Id)
    | GotViewport Viewport
    | OnDragBy Vec2
    | StartDragging String
    | StopDragging
    | ToggleBoxClicked String
    | Zoom FullZoom


boxPositions : List Vec2
boxPositions =
    let
        indexToPosition =
            toFloat >> (*) 60 >> (+) 10 >> Vector2.vec2 10
    in
    List.range 0 10 |> List.map indexToPosition


init : flags -> ( Model, Cmd Msg )
init _ =
    ( { zoom = 1
      , panning = False
      , center = Vector2.vec2 0 0 
      , boxGroup = makeBoxGroup boxPositions
      , drag = Draggable.init
      , viewport = Nothing
      }
    , Task.perform GotViewport Browser.Dom.getViewport
    )


dragConfig : Draggable.Config Id Msg
dragConfig =
    Draggable.customConfig
        [ onDragBy (\( dx, dy ) -> Vector2.vec2 dx dy |> OnDragBy)
        , onDragStart StartDragging
        , onClick ToggleBoxClicked
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ boxGroup, zoom, center, panning } as model) =
    case msg of
        GotViewport viewport ->
            ({model | viewport = Just viewport}, Cmd.none)

        OnDragBy delta ->
            let 
                newDelta = delta |> Vector2.scale (1/zoom)
            in 
                if panning then
                    ( { model | center = center |> Vector2.add (newDelta |> Vector2.scale -1) }, Cmd.none )
                else
                    ( { model | boxGroup = boxGroup |> dragActiveBy newDelta }, Cmd.none )

        StartDragging id ->
            if id == "pan" then
                ( { model | panning = True }, Cmd.none )
            else
                ( { model | panning = False, boxGroup = boxGroup |> startDragging id }, Cmd.none )

        StopDragging ->
            if panning then
                ( { model | panning = False }, Cmd.none )
            else
                ( { model | panning = False, boxGroup = boxGroup |> stopDragging }, Cmd.none )

        ToggleBoxClicked id ->
            ( { model | boxGroup = boxGroup |> toggleBoxClicked id }, Cmd.none )

        Zoom {scaleFactor, zoomCenter} ->
            let
                halfScreen = getViewportCenter model.viewport
                newZoom = zoom
                          |> (+) scaleFactor
                          |> clamp 0.5 5
                baseDelta = zoomCenter 
                            |> Vector2.sub halfScreen 
                            |> Vector2.scale (-1/zoom+1/newZoom)
            in
            ( { model | zoom = newZoom , center = center |> Vector2.add baseDelta}, Cmd.none )

        DragMsg dragMsg ->
            Draggable.update dragConfig dragMsg model



subscriptions : Model -> Sub Msg
subscriptions { drag } =
    Draggable.subscriptions DragMsg drag



-- VIEW


boxSize : Vec2
boxSize =
    Vector2.vec2 50 50


view : Model -> Html Msg
view { zoom, center, boxGroup, viewport } =
    let 
        viewCenter = getViewportCenter viewport
        ( cx, cy ) =
            ( getX center, getY center )
        (vCx, vCy) =
            ( (getX viewCenter), (getY viewCenter) )
        ( top, left ) =
            ( cy - vCy, cx - vCx )
        ( bottom, right ) =
            ( cy + vCy, cx + vCx )

        panning =
            "translate(" ++ String.fromFloat vCx ++ ", " ++ String.fromFloat vCy ++ ")"

        zooming =
            "scale(" ++ String.fromFloat zoom ++ ")"
    in
        Html.div
            [ handleZoom Zoom
            , Draggable.mouseTrigger "pan" DragMsg
            , Html.Attributes.style "height" "100vh"]
            [ Html.p
                [ Html.Attributes.style "padding-left" "8px" ]
                [ Html.text ("X:"++ String.fromFloat vCx ++ "px Y:" ++ String.fromFloat vCy) ]
              , boxesView zoom center (getViewportCenter viewport)  boxGroup   
            ]

getViewportCenter : Maybe Viewport -> Vec2
getViewportCenter maybeViewport =
    Vector2.scale 0.5
        <| case maybeViewport of 
                Nothing -> vec2 0 0
                Just viewport -> vec2 viewport.viewport.width viewport.viewport.height


boxesView : Float -> Vec2 -> Vec2 -> BoxGroup -> Html Msg
boxesView zoom center viewCenter boxGroup =
    let 
        boxList = boxGroup
                    |> allBoxes
                    |> List.reverse
        zoomList = List.repeat (List.length boxList) zoom
        centerList = List.repeat (List.length boxList) center
        viewCenterList = List.repeat (List.length boxList) viewCenter
    in 
        List.map4 boxView zoomList centerList viewCenterList boxList
            |> Html.node "g" []


boxView : Float -> Vec2 -> Vec2 -> Box -> Html Msg
boxView zoom center viewCenter {id, position, clicked} =
    let
        ( cx, cy ) =
            ( getX center, getY center )
        (vCx, vCy) =
            ( (getX viewCenter)/zoom, (getY viewCenter)/zoom )
        ( top, left ) =
            ( cy - vCy, cx - vCx )

        color =
            if clicked then
                "red"

            else
                "lightblue"
        translate = 
            "translate(" ++ String.fromFloat ((getX position)-left) 
                         ++ "px, " 
                         ++ String.fromFloat ((getY position)-top)
                         ++ "px)"
        zooming = 
            "scale(" ++ (String.fromFloat zoom) ++ ")"
    in
    Html.div
        ([ Html.Attributes.style "transform" (zooming ++ " " ++ translate)
         , Html.Attributes.style "position" "absolute"
         , Html.Attributes.style "padding" "16px"
         , Html.Attributes.style "background-color" color
         , Html.Attributes.style "width" "64px"
         , Html.Attributes.style "cursor" "move"
         , Draggable.mouseTrigger id DragMsg
         , onMouseUp StopDragging
         ]
            ++ Draggable.touchTriggers id DragMsg
        )
        [ Html.text "Drag me" ]


decodeOffset : Decoder Vec2
decodeOffset =
    Decode.map2 vec2
        (Decode.field "clientX" Decode.float)
        (Decode.field "clientY" Decode.float)

decodeZoom : Decoder Float
decodeZoom =
    Decode.map scrollToZoom
        (Decode.field "deltaY" Decode.float)

decodeFullZoom : Decoder FullZoom
decodeFullZoom =
    Decode.map2 FullZoom
        decodeZoom 
        decodeOffset


scrollToZoom : Float -> Float
scrollToZoom scroll =
    negate (scroll * 0.02)

handleZoom : (FullZoom -> msg) -> Svg.Attribute msg
handleZoom onZoom =
    let
        alwaysPreventDefaultAndStopPropagation msg =
            { message = msg, stopPropagation = True, preventDefault = True }

        zoomDecoder : Decoder msg
        zoomDecoder =
            decodeFullZoom
                |> Decode.map onZoom
    in
    Html.Events.custom
        "wheel"
    <|
        Decode.map alwaysPreventDefaultAndStopPropagation zoomDecoder

num : (String -> Svg.Attribute msg) -> Float -> Svg.Attribute msg
num attr value =
    attr (String.fromFloat value)
